package ukdw.com.progmob_2020.Crud;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);
        final EditText edNidnCari = (EditText)findViewById(R.id.edNidnCari);
        final EditText edNamaDosen = (EditText)findViewById(R.id.edNamaDosenBaru);
        final EditText edNidn = (EditText)findViewById(R.id.edNidnBaru);
        final EditText edAlamatBaru = (EditText)findViewById(R.id.edAlamatBaru);
        final EditText edEmailBaru = (EditText)findViewById(R.id.edEmailBaru);
        final EditText edgelar = (EditText)findViewById(R.id.editGelardsn);
        Button btnUbah = (Button) findViewById(R.id.btnUbah);
        pd = new ProgressDialog(DosenUpdateActivity.this);

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> upd = service.update_dsn(
                        edNidnCari.getText().toString(),
                        edNamaDosen.getText().toString(),
                        edNidn.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        edgelar.getText().toString(),
                        "kosong",
                        "72180268"
                );

                upd.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(DosenUpdateActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this,"Error",Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }
}