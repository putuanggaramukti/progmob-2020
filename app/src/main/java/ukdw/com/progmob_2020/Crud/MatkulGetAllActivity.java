package ukdw.com.progmob_2020.Crud;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.MatkulCRUDRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;

import ukdw.com.progmob_2020.R;

public class MatkulGetAllActivity extends AppCompatActivity {
    RecyclerView rvMtkl;
    MatkulCRUDRecyclerAdapter mtklAdapter;
    ProgressDialog pd;
    List<Matkul> matkulList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_get_all);

        rvMtkl =(RecyclerView)findViewById(R.id.rvGetMatkul);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matkul>> call = service.getMatkul("72180268");

        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss();
                matkulList = response.body();
                mtklAdapter = new MatkulCRUDRecyclerAdapter(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MatkulGetAllActivity.this);
                rvMtkl.setLayoutManager(layoutManager);
                rvMtkl.setAdapter(mtklAdapter);
            }

            @Override
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MatkulGetAllActivity.this, "Error",Toast.LENGTH_LONG);
            }
        });
    }
}