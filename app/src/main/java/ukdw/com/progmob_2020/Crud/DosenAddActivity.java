package ukdw.com.progmob_2020.Crud;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_add);

        final EditText ednama = (EditText)findViewById(R.id.editNamadsn);
        final EditText ednidn = (EditText)findViewById(R.id.editNidn);
        final EditText edalamat = (EditText)findViewById(R.id.editAlamatdsn);
        final EditText edemail = (EditText)findViewById(R.id.editEmaildsn);
        final EditText edgelar = (EditText)findViewById(R.id.editGelardsn);
        Button btnsimpan = (Button)findViewById(R.id.btnUpdatedatadsn);
        pd = new ProgressDialog(DosenAddActivity.this);

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dsn(
                        ednama.getText().toString(),
                        ednidn.getText().toString(),
                        edalamat.getText().toString(),
                        edemail.getText().toString(),
                        edgelar.getText().toString(),
                        "",
                        "72180225"
                );


                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "Data Berhasil di Simpan", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DosenAddActivity.this, MainDosenActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "ERROR", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}