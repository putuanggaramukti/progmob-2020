package ukdw.com.progmob_2020;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import ukdw.com.progmob_2020.Crud.MainDosenActivity;
import ukdw.com.progmob_2020.Crud.MainMatkulActivity;
import ukdw.com.progmob_2020.Crud.MainMhsActivity;

public class HomeActivity extends AppCompatActivity {
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toast.makeText(HomeActivity.this, "Halaman Home", Toast.LENGTH_SHORT).show();
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);
        if(sharedpreferences.getString("nimnik", "").isEmpty() && sharedpreferences.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            return;
        }

        Button btnMhs = (Button)findViewById(R.id.btnMhs);
        Button btnDosen = (Button)findViewById(R.id.btnLihatDosen);
        Button btnMatkul = (Button)findViewById(R.id.btnMatkul);
        Button btnLogout = (Button)findViewById(R.id.btnLogout);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.apply();
                finish();
                Intent Intent = new Intent(HomeActivity.this,LoginActivity.class);
                startActivity(Intent);
            }
        });

        btnMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });

        btnDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });

        btnMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });
    }
}