package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Matkul;
import ukdw.com.progmob_2020.R;

public class MatkulCRUDRecyclerAdapter extends RecyclerView.Adapter<MatkulCRUDRecyclerAdapter.viewHolder> {
    private Context context;
    private List<Matkul> matkulList;

    public MatkulCRUDRecyclerAdapter(Context context){
        this.context = context;
        matkulList = new ArrayList<>();
    }

    public MatkulCRUDRecyclerAdapter(List<Matkul> matkulList){
        this.matkulList = matkulList;
    }

    public List<Matkul> getMatkulList() {
        return matkulList;
    }

    public void setMatkulList(List<Matkul> matkulList) {
        this.matkulList = matkulList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul,parent,false);
        return new viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        Matkul mk = matkulList.get(position);
        holder.tvnama.setText(mk.getNama());
        holder.tvkodemk.setText(mk.getKode());
        holder.tvharimk.setText(mk.getHari());
        holder.tvsesimk.setText(mk.getSesi());
        holder.tvsksmk.setText(mk.getSks());
        holder.mk = mk;
    }

    @Override
    public int getItemCount() {
        return matkulList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        private TextView tvnama, tvkodemk, tvharimk, tvsesimk, tvsksmk;
        private RecyclerView rvMatakuliah;
        Matkul mk;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            tvnama = itemView.findViewById(R.id.tvnamamk);
            tvkodemk = itemView.findViewById(R.id.tvkodemk);
            tvharimk = itemView.findViewById(R.id.tvharimk);
            tvsesimk = itemView.findViewById(R.id.tvsesimk);
            tvsksmk = itemView.findViewById(R.id.tvsksmk);
            rvMatakuliah = itemView.findViewById(R.id.rvGetMatkul);
        }
    }
}

