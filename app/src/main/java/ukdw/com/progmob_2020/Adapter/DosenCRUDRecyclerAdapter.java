package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.R;

public class DosenCRUDRecyclerAdapter extends RecyclerView.Adapter<DosenCRUDRecyclerAdapter.viewHolder> {
    private Context context;
    private List<Dosen> dosenList;

    public DosenCRUDRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosenCRUDRecyclerAdapter(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_dosen,parent,false);
        return new viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DosenCRUDRecyclerAdapter.viewHolder holder, int position) {
        Dosen d = dosenList.get(position);
        holder.tvNamaDosen.setText(d.getNama());
        holder.tvNidn.setText(d.getNidn());
        holder.tvAlamat.setText(d.getAlamat());
        holder.tvEmail.setText(d.getEmail());
        holder.tvGelar.setText(d.getGelar());
        holder.d = d;
    }

    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        private TextView tvNamaDosen,tvNidn, tvEmail, tvGelar, tvAlamat;
        private RecyclerView rvGetDosen;
        Dosen d;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            tvNamaDosen = itemView.findViewById(R.id.tvNamaDosen);
            tvNidn = itemView.findViewById(R.id.tvNidn);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            tvGelar = itemView.findViewById(R.id.tvGelar);
            tvAlamat = itemView.findViewById(R.id.tvAlamat);
            rvGetDosen = itemView.findViewById(R.id.rvGetDosen);

        }
    }


}
