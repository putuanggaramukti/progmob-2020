package ukdw.com.progmob_2020.Pertemuan2;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_test);
        CardView cv = (CardView)findViewById(R.id.cvMahasiswa);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;


        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();
        Mahasiswa m1 = new Mahasiswa("Putu","72180268","081");
        Mahasiswa m2 = new Mahasiswa("Effandito","72180263","082");
        Mahasiswa m3 = new Mahasiswa("Davine","72180001","083");
        Mahasiswa m4 = new Mahasiswa("Adrian","72180002","084");
        Mahasiswa m5 = new Mahasiswa("Jeffry","72180003","085");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(CardViewTestActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);


    }
}